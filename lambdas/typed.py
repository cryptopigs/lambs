""" run-time types for lambda calculus

Typed object is a pair of type and a value:
typed_obj = [type, value]

Operations with run-time type checks:
1. check arg types
2. extract untyped values
3. carry out untyped operations
4. make typed result

Each type should have:
constructor - to make instances of this type of untyped values, e.g. MAKE_NUMBER(untyped.ZERO)
type checker - function to check if its argument has this type, e.g. IS_NUMBER
error object - to use in case of type errors, e.g. NUMBER_ERROR
"""

from lambdas import untyped as uu

GET_TYPE = lambda x: x(uu.TRUE)
GET_VALUE = lambda x: x(uu.FALSE)
IS_TYPE = lambda t: lambda obj: (
    uu.ISEQUAL
    (GET_TYPE(obj))
    (t)
)

# represent types as numbers
ERROR_TYPE = uu.ZERO
BOOL_TYPE = uu.NEXT(uu.ZERO)  # 1
NUMBER_TYPE = uu.NEXT(uu.NEXT(uu.ZERO))  # 2
LIST_TYPE = uu.NEXT(uu.NEXT(uu.NEXT(uu.ZERO)))  # 3

# creates error object of ERROR_TYPE and some value
MAKE_ERROR = uu.PAIR(ERROR_TYPE)  # λv.[ERROR_TYPE, v]

# errors
# type error - is an error object that has ERROR_TYPE type and an expected type as a value
ERROR = MAKE_ERROR(ERROR_TYPE)  # universal error object: [ERROR_TYPE, ERROR_TYPE]
IS_ERROR = IS_TYPE(ERROR_TYPE)

# universal function to make typed version out of untyped function of one argument
MAKE_TYPED_FUN = (
    lambda type_checker:  # fun that checks if its argument has correct type
    lambda constructor:  # fun that creates a [type, value] pair
    lambda type_error:  # return it in case of type error
    lambda untyped_fun:
    lambda x: (
        uu.IF(type_checker(x))  # check arg types
        (constructor(untyped_fun(GET_VALUE(x))))  # get untyped value, do untyped operation, make typed result
        (type_error)  # else
    )
)

# universal function to make typed version out of untyped function of two arguments
MAKE_TYPED_FUN2 = (
    lambda type_checker:
    lambda constructor:
    lambda type_error:
    lambda untyped_fun:
    lambda x: lambda y: (
        uu.IF(uu.AND(type_checker(x))(type_checker(y)))
        (
            constructor(
                untyped_fun(GET_VALUE(x))(GET_VALUE(y))
            )
        )
        (type_error)
    )
)

# 1. bools
# can only be constants TRUE FALSE
MAKE_BOOL = uu.PAIR(BOOL_TYPE)
IS_BOOL = IS_TYPE(BOOL_TYPE)
BOOL_ERROR = MAKE_ERROR(BOOL_TYPE)

TRUE = MAKE_BOOL(uu.TRUE)
FALSE = MAKE_BOOL(uu.FALSE)
NOT = MAKE_TYPED_FUN(IS_BOOL)(MAKE_BOOL)(BOOL_ERROR)(uu.NOT)
AND = MAKE_TYPED_FUN2(IS_BOOL)(MAKE_BOOL)(BOOL_ERROR)(uu.AND)

# return typed TRUE/FALSE constant if x of bool type behaves like TRUE/FALSE
GET_BOOL_CONST = lambda x: (
    uu.IF(IS_BOOL(x))
    (
        GET_VALUE(x)(TRUE)(FALSE)
    )
    (BOOL_ERROR)
)

# 2. numbers
MAKE_NUMBER = uu.PAIR(NUMBER_TYPE)
IS_NUMBER = IS_TYPE(NUMBER_TYPE)
NUMBER_ERROR = MAKE_ERROR(NUMBER_TYPE)

ZERO = MAKE_NUMBER(uu.ZERO)
ISZERO = MAKE_TYPED_FUN(IS_NUMBER)(MAKE_BOOL)(NUMBER_ERROR)(uu.ISZERO)
NEXT = MAKE_TYPED_FUN(IS_NUMBER)(MAKE_NUMBER)(NUMBER_ERROR)(uu.NEXT)
PREV = MAKE_TYPED_FUN(IS_NUMBER)(MAKE_NUMBER)(NUMBER_ERROR)(uu.PREV)

SUM = MAKE_TYPED_FUN2(IS_NUMBER)(MAKE_NUMBER)(NUMBER_ERROR)(uu.SUM)
SUB = MAKE_TYPED_FUN2(IS_NUMBER)(MAKE_NUMBER)(NUMBER_ERROR)(uu.SUB)
ABS_DIFF = MAKE_TYPED_FUN2(IS_NUMBER)(MAKE_NUMBER)(NUMBER_ERROR)(uu.ABS_DIFF)

ISEQUAL = MAKE_TYPED_FUN2(IS_NUMBER)(
    lambda x: GET_BOOL_CONST(MAKE_BOOL(x))  # construct bool and return TRUE or FALSE constant
)(NUMBER_ERROR)(uu.ISEQUAL)

# 3. lists
# can be empty or a PAIR(head)(tail)
# head - any element, tail - is a list
MAKE_LIST = uu.PAIR(LIST_TYPE)
IS_LIST = IS_TYPE(LIST_TYPE)
LIST_ERROR = MAKE_ERROR(LIST_TYPE)

# empty list will have head and tail set to LIST_ERROR
NIL = MAKE_LIST(
    uu.PAIR(LIST_ERROR)(LIST_ERROR)
)

CONS = lambda h: lambda t: (
    uu.IF(IS_LIST(t))
    (MAKE_LIST(uu.PAIR(h)(t)))
    (LIST_ERROR)
)

HEAD = lambda l: (
    uu.IF(IS_LIST(l))
    (GET_VALUE(l)(uu.TRUE))  # get first element of the pair
    (LIST_ERROR)
)

TAIL = lambda l: (
    uu.IF(IS_LIST(l))
    (GET_VALUE(l)(uu.FALSE))  # get second element of the pair
    (LIST_ERROR)
)

IS_NIL = lambda l: (
    uu.IF(IS_LIST(l))
    (MAKE_BOOL(IS_ERROR(HEAD(l))))
    (LIST_ERROR)
)

# len = λl.if (l is nil) then zero else next len(tail l)
# len = [λf.λl.if (l is nil) then zero else next f(tail l)] len
LEN_AUX = lambda f: lambda l: (
    uu.IF(GET_VALUE(IS_NIL(l)))
    (ZERO)
    (lambda z: NEXT(f(TAIL(l)))(z))
)
LEN = uu.Z(LEN_AUX)


def lamblist_of_list(l, acc=NIL):
    """ convert list to lambda list """

    if len(l) == 0:
        return acc
    else:
        head = l[-1]
        tail = l[:-1]
        return lamblist_of_list(tail, acc=CONS(head)(acc))


def list_of_lamblist(lam_l, acc=None):
    """ convert lambda list to list """

    if acc is None:
        acc = []

    if lam_l == NIL:
        return acc
    else:
        return list_of_lamblist(TAIL(lam_l), acc + [HEAD(lam_l)])
