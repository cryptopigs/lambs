""" yet another lambda calculus in Python """

# boolean operations
TRUE = lambda x: lambda y: x
FALSE = lambda x: lambda y: y
NOT = lambda x: x(FALSE)(TRUE)  # λx.x FALSE TRUE
AND = lambda x: lambda y: x(y)(x)  # λxy.x y x,  λxy.x y FALSE

# return TRUE/FALSE constant if x behaves like TRUE/FALSE
GET_BOOL_CONST = lambda x: x(TRUE)(FALSE)

# combinators
I = lambda x: x            # Idiot/Identity I = λx.x
K = lambda x: lambda y: x  # Kestrel/Const/True K = λxy.x
KI = K(I)                  # Kite/Second/False KI = K I

# IF CONDITION THEN_BRANCH ELSE_BRANCH
# λbxy.bxy
IF = lambda b: lambda x: lambda y: b(x)(y)

# Y-combinator
# loops forever in Python, Y = λf.(λx.f(xx))(λx.f(xx))
# Y = lambda f: (
#     (lambda x: f(x(x)))
#     (lambda x: f(x(x)))
# )

# Z-combinator
# uses η-expansion: A B = (λx.Ax)B => A = λx.Ax
# Y[xx -> λy.xxy] -> Z = λf.(λx.f(λy.xxy))(λx.f(λy.xxy))
Z = lambda f: (
    (lambda x: f(lambda y: x(x)(y)))
    (lambda x: f(lambda y: x(x)(y)))
)

# tuple [u, v] = λz.(zu)v
PAIR = lambda u: lambda v: lambda z: z(u)(v)

# natural numbers (not Church)
# https://www.macs.hw.ac.uk/~greg/books/gjm.lambook88.pdf
# d0 = I
# d1 = [F, d0]
# d2 = [F, d1]
# ...
# d(n+1) = [F, dn]
ZERO = I
NEXT = lambda x: PAIR(FALSE)(x)  # λx.[F, x]
ISZERO = lambda x: x(TRUE)       # λx.xT
# PREV = lambda x: x(FALSE)      # λx.xF
# PREV(ZERO) = ZERO, in order to make SUB work
PREV = lambda x: (
    IF(ISZERO(x))
    (ZERO)
    (x(FALSE))
)

# F X = X  (1) definition of fixpoint
# X = Y F  (2) how Y-combinator works

# SUM by definition is:
# SUM = λxy.if (iszero y) x (SUM (next x) (prev y))

# SUM = (λfxy.if (iszero y) x (f (next x) (prev y))) SUM
# SUM_AUX = λfxy.if (iszero y) x (f (next x) (prev y))

# SUM = SUM_AUX SUM
# (1), (2) => SUM = Y SUM_AUX

SUM_AUX = lambda f: lambda x: lambda y: (
    IF(ISZERO(y))
    (lambda z: x(z))  # expansion
    (lambda z: (f(NEXT(x))(PREV(y)))(z))
)

SUM = Z(SUM_AUX)

# natural subtraction: x - y = 0, if y > x
SUB_AUX = lambda f: lambda x: lambda y: (
    IF(ISZERO(y))
    (lambda z: x(z))
    (lambda z: (f(PREV(x))(PREV(y)))(z))
)

SUB = Z(SUB_AUX)

# abs(x - y)
ABS_DIFF = lambda x: lambda y: (
    SUM(SUB(x)(y))(SUB(y)(x))
)

# equality of natural numbers: x equals y, if abs(x - y) = 0
ISEQUAL = lambda x: lambda y: (
    ISZERO(ABS_DIFF(x)(y))
)


def int_of_lambnum(lam_num, acc=0):
    """ convert lambda natural numbers to int natural numbers """
    if ISZERO(lam_num) == TRUE:
        return acc
    else:
        return int_of_lambnum(PREV(lam_num), acc + 1)


def lambnum_of_int(n, acc=ZERO):
    """ convert int natural numbers to lambda natural numbers """
    if n == 0:
        return acc
    else:
        return lambnum_of_int(n - 1, NEXT(acc))
