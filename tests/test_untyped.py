import pytest
from lambdas import untyped as uu


def test_not():
    """ test boolean NOT function """
    assert uu.NOT(uu.TRUE) is uu.FALSE
    assert uu.NOT(uu.FALSE) is uu.TRUE


def test_and():
    """ test boolean AND function """
    assert uu.AND(uu.TRUE)(uu.TRUE) is uu.TRUE
    assert uu.AND(uu.TRUE)(uu.FALSE) is uu.FALSE
    assert uu.AND(uu.FALSE)(uu.TRUE) is uu.FALSE
    assert uu.AND(uu.FALSE)(uu.FALSE) is uu.FALSE


def test_kite():
    """ test Kite combinator behaves like False """
    assert uu.NOT(uu.KI) == uu.TRUE  # works with logic operations
    assert uu.KI('x')('y') == 'y'  # returns second argument
    assert uu.IF(uu.KI)('x')('y') == 'y'  # works in IF
    assert uu.PAIR('x')('y')(uu.KI) == 'y'  # returns second value from pair
    assert uu.GET_BOOL_CONST(uu.KI) is uu.FALSE


def test_get_bool_const():
    """ test that GET_BOOL_CONST simplifies expression to TRUE/FALSE """
    assert uu.GET_BOOL_CONST(lambda x: lambda y: x) is uu.TRUE
    assert uu.GET_BOOL_CONST(lambda x: lambda y: y) is uu.FALSE


def test_if():
    """ test IF statement """
    assert uu.IF(uu.TRUE)('x')('y') == 'x'
    assert uu.IF(uu.FALSE)('x')('y') == 'y'


@pytest.mark.parametrize(
    'fun, arg, expected',
    [
        (lambda f: lambda n: 1 if n < 2 else n * f(n - 1), 7, 5040),
        (lambda f: lambda n: 1 if n < 3 else f(n - 1) + f(n - 2), 10, 55)
    ],  # slow, non-tail recursion
    ids=['factorial(n)', 'Fibonacci nth number']
)
def test_z_combinator(fun, arg, expected):
    """ test recursion with Z-combinator """
    assert uu.Z(fun)(arg) == expected


def test_numbers_converters():
    """ test int_of_num() and num_of_int() """
    assert uu.int_of_lambnum(uu.ZERO) == 0
    assert uu.int_of_lambnum(uu.NEXT(uu.ZERO)) == 1
    assert uu.int_of_lambnum(uu.NEXT(uu.lambnum_of_int(10))) == 11
    assert uu.PREV(uu.lambnum_of_int(1)) is uu.ZERO
    assert uu.lambnum_of_int(0) is uu.ZERO


def test_prev():
    """ test PREV with constructive specifications """
    # base case for base object
    assert uu.ISEQUAL(uu.PREV(uu.ZERO))(uu.ZERO) is uu.TRUE

    # "general" case for constructive object
    n1 = uu.lambnum_of_int(123)
    assert uu.ISEQUAL(uu.PREV(uu.NEXT(n1)))(n1) is uu.TRUE


def test_iszero():
    """ test ISZERO function"""
    assert uu.ISZERO(uu.ZERO) is uu.TRUE
    assert uu.ISZERO(uu.PREV(uu.ZERO)) is uu.TRUE  # PREV ZERO should be ZERO
    assert uu.ISZERO(uu.NEXT(uu.ZERO)) is uu.FALSE


@pytest.mark.parametrize(
    'n1, n2',
    [
        (0, 0),
        (0, 12),
        (12, 0),
        (123, 456),
    ])
def test_sum(n1, n2):
    """ test SUM of natural numbers """
    d1 = uu.lambnum_of_int(n1)
    d2 = uu.lambnum_of_int(n2)
    result = uu.SUM(d1)(d2)
    assert uu.int_of_lambnum(result) == n1 + n2
    assert uu.ISEQUAL(result)(uu.lambnum_of_int(n1 + n2)) is uu.TRUE


@pytest.mark.parametrize(
    'n1, n2',
    [
        (0, 0),
        (0, 5),
        (1, 5),
        (5, 0),
        (5, 2),
        (5, 5),
    ])
def test_sub(n1, n2):
    """ test SUB of natural numbers """
    d1 = uu.lambnum_of_int(n1)
    d2 = uu.lambnum_of_int(n2)
    result = uu.SUB(d1)(d2)
    assert uu.int_of_lambnum(result) == (n1 - n2 if n1 >= n2 else 0)
    assert uu.ISEQUAL(result)(uu.lambnum_of_int(n1 - n2 if n1 >= n2 else 0)) is uu.TRUE


@pytest.mark.parametrize(
    'n1, n2',
    [
        (0, 0),
        (0, 5),
        (1, 5),
        (5, 0),
        (5, 2),
        (5, 5),
    ])
def test_abs_diff(n1, n2):
    """ test ABS_DIFF of natural numbers """
    d1 = uu.lambnum_of_int(n1)
    d2 = uu.lambnum_of_int(n2)
    result = uu.ABS_DIFF(d1)(d2)
    assert uu.int_of_lambnum(result) == abs(n1 - n2)


def test_equal_positive():
    """ test ISEQUAL returns TRUE for equal numbers"""
    n10, n12, n1, n0 = (uu.lambnum_of_int(n) for n in [10, 12, 1, 0])

    assert uu.ISEQUAL(n0)(n0) is uu.TRUE  # 0 == 0
    assert uu.ISEQUAL(uu.NEXT(n10))(uu.PREV(n12)) is uu.TRUE  # 10 + 1 == 12 - 1
    assert uu.ISEQUAL(
        uu.SUB(n12)(n10)  # 12 - 10
    )(
        uu.SUM(n1)(n1)  # 1 + 1
    ) is uu.TRUE


def test_equal_negative():
    """ test ISEQUAL returns FALSE for non-equal numbers"""
    n10, n12, n0 = (uu.lambnum_of_int(n) for n in [10, 12, 0])
    assert uu.ISEQUAL(n0)(n10) is uu.FALSE
    assert uu.ISEQUAL(n10)(n0) is uu.FALSE
    assert uu.ISEQUAL(n10)(n12) is uu.FALSE
