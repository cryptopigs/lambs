import pytest
from lambdas import untyped as uu, typed as tl


def test_error():
    """ test universal error object """
    assert tl.IS_ERROR(tl.ERROR) is uu.TRUE  # uses GET_TYPE and IS_TYPE
    assert tl.GET_VALUE(tl.ERROR) is tl.ERROR_TYPE


@pytest.mark.parametrize(
    'x, y',
    [
        (tl.TRUE, tl.FALSE),
        (tl.FALSE, tl.TRUE),
        (tl.ZERO, tl.BOOL_ERROR),
    ],
    ids=[
        'not True == False',
        'not False == True',
        'not Zero == BOOL_ERROR',
    ]
)
def test_not(x, y):
    """ test typed boolean NOT function """
    assert tl.GET_BOOL_CONST(tl.NOT(x)) == y


@pytest.mark.parametrize(
    'x, y, z',
    [
        (tl.TRUE, tl.TRUE, tl.TRUE),
        (tl.TRUE, tl.FALSE, tl.FALSE),
        (tl.FALSE, tl.TRUE, tl.FALSE),
        (tl.FALSE, tl.FALSE, tl.FALSE),
        (tl.ZERO, tl.TRUE, tl.BOOL_ERROR),
        (tl.TRUE, tl.ZERO, tl.BOOL_ERROR),
    ],
    ids=[
        'True and True == True',
        'True and False == False',
        'False and True == False',
        'False and False == False',
        'Zero and True == BOOL_ERROR',
        'True and Zero == BOOL_ERROR',
    ]
)
def test_and(x, y, z):
    """ test typed boolean AND function """
    assert tl.GET_BOOL_CONST(tl.AND(x)(y)) == z


@pytest.mark.parametrize(
    'x, y',
    [
        (tl.ZERO, tl.TRUE),
        (tl.PREV(tl.ZERO), tl.TRUE),
        (tl.NEXT(tl.ZERO), tl.FALSE),
        (tl.TRUE, tl.BOOL_ERROR),
    ],
    ids=[
        'iszero(zero) == True',
        'iszero(prev(zero)) == True',
        'iszero(next(zero)) == False',
        'iszero(True) == BOOL_ERROR',
    ]
)
def test_iszero(x, y):
    """ test ISZERO function"""
    assert tl.GET_BOOL_CONST(tl.ISZERO(x)) == y


@pytest.mark.parametrize(
    'n1, n2',
    [
        (0, 0),
        (0, 12),
        (12, 0),
        (123, 456),
    ])
def test_sum_positive(n1, n2):
    """ test SUM of natural numbers """
    d1 = tl.MAKE_NUMBER(uu.lambnum_of_int(n1))
    d2 = tl.MAKE_NUMBER(uu.lambnum_of_int(n2))

    result = tl.SUM(d1)(d2)
    assert uu.int_of_lambnum(tl.GET_VALUE(result)) == n1 + n2


def test_sum_negative():
    """ SUM with wrong types returns NUMBER_ERROR """
    d1 = tl.MAKE_NUMBER(uu.lambnum_of_int(123))
    d2 = tl.TRUE

    result = tl.SUM(d1)(d2)
    assert result is tl.NUMBER_ERROR


@pytest.mark.parametrize(
    'n1, n2',
    [
        (0, 0),
        (0, 5),
        (1, 5),
        (5, 0),
        (5, 2),
        (5, 5),
    ])
def test_sub_positive(n1, n2):
    """ test SUB of natural numbers """
    d1 = tl.MAKE_NUMBER(uu.lambnum_of_int(n1))
    d2 = tl.MAKE_NUMBER(uu.lambnum_of_int(n2))

    result = tl.SUB(d1)(d2)
    assert uu.int_of_lambnum(tl.GET_VALUE(result)) == (n1 - n2 if n1 >= n2 else 0)


def test_sub_negative():
    """ SUB with wrong types returns NUMBER_ERROR """
    d1 = tl.MAKE_NUMBER(uu.lambnum_of_int(123))
    d2 = tl.TRUE

    result = tl.SUB(d1)(d2)
    assert result is tl.NUMBER_ERROR


@pytest.mark.parametrize(
    'n1, n2',
    [
        (0, 0),
        (0, 5),
        (1, 5),
        (5, 0),
        (5, 2),
        (5, 5),
    ])
def test_abs_diff(n1, n2):
    """ test ABS_DIFF of natural numbers """
    d1 = tl.MAKE_NUMBER(uu.lambnum_of_int(n1))
    d2 = tl.MAKE_NUMBER(uu.lambnum_of_int(n2))
    result = tl.ABS_DIFF(d1)(d2)
    assert uu.int_of_lambnum(tl.GET_VALUE(result)) == abs(n1 - n2)


def test_equal_positive():
    """ test ISEQUAL returns TRUE for equal numbers """
    n10, n12, n1, n0 = (
        tl.MAKE_NUMBER(uu.lambnum_of_int(n)) for n in [10, 12, 1, 0]
    )

    assert tl.ISEQUAL(n0)(n0) is tl.TRUE  # 0 == 0
    assert tl.ISEQUAL(tl.NEXT(n10))(tl.PREV(n12)) is tl.TRUE  # 10 + 1 == 12 - 1
    assert tl.ISEQUAL(
        tl.SUB(n12)(n10)  # 12 - 10
    )(
        tl.SUM(n1)(n1)  # 1 + 1
    ) is tl.TRUE


def test_equal_negative():
    """ test ISEQUAL returns FALSE for non-equal numbers """
    n10, n12, n0 = (
        tl.MAKE_NUMBER(uu.lambnum_of_int(n)) for n in [10, 12, 0]
    )
    assert tl.ISEQUAL(n0)(n10) is tl.FALSE
    assert tl.ISEQUAL(n10)(n0) is tl.FALSE
    assert tl.ISEQUAL(n10)(n12) is tl.FALSE
    assert tl.ISEQUAL(tl.ZERO)(tl.TRUE) is tl.NUMBER_ERROR


def test_list_converters():
    """ test lamblist_of_list() and list_of_lamblist() """

    # test empty list <=> NIL
    assert tl.list_of_lamblist(tl.NIL) == []
    assert tl.lamblist_of_list([]) is tl.NIL

    # create lambda list with CONS
    # test converted lambda list equals regular list
    lam_list = (
        tl.CONS(20)(
            tl.CONS(30)(
                tl.CONS(40)(
                    tl.NIL
                )
            )
        )
    )

    assert tl.list_of_lamblist(lam_list) == [20, 30, 40]

    # create lamb list out of list
    # test its head and tail
    lam_list1 = tl.lamblist_of_list([10, 20, 30, 40])
    assert tl.HEAD(lam_list1) == 10
    assert tl.list_of_lamblist(tl.TAIL(lam_list1)) == [20, 30, 40]


def test_typed_cons():
    """ test creating lists with CONS """
    # list with one element
    el0 = tl.FALSE
    l = tl.CONS(el0)(tl.NIL)

    assert tl.IS_LIST(l) is uu.TRUE
    assert tl.HEAD(l) == el0
    assert tl.TAIL(l) == tl.NIL

    # second arg is not a list
    bad_list = tl.CONS(el0)(el0)
    assert bad_list == tl.LIST_ERROR


@pytest.mark.parametrize(
    'lam_list, bool_result',
    [
        (tl.lamblist_of_list([]), tl.TRUE),
        (tl.NIL, tl.TRUE),
        (tl.CONS(tl.ZERO)(tl.NIL), tl.FALSE),
    ]
)
def test_is_nil(lam_list, bool_result):
    """ test IS_NIL """
    r = tl.IS_NIL(lam_list)
    assert tl.GET_BOOL_CONST(r) is bool_result


@pytest.mark.parametrize(
    'l',
    [
        [],
        [tl.ZERO, tl.ZERO, tl.ZERO],
    ]
)
def test_len(l):
    """ test LEN """
    lam_list = tl.lamblist_of_list(l)
    length = tl.LEN(lam_list)
    length_expected = tl.MAKE_NUMBER(uu.lambnum_of_int(len(l)))
    assert tl.ISEQUAL(length)(length_expected) is tl.TRUE
